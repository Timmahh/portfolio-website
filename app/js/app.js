var app = angular.module('portfolioApp', ['ngRoute', 'ui.bootstrap']);

app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'views/home.html',
			controller  : 'homeController'
		})
		.when('/about', {
			templateUrl : 'views/about.html',
			controller  : 'aboutController'
		})
		.when('/portfolio', {
			templateUrl : 'views/portfolio.html',
			controller  : 'portfolioController'
		})			
		.when('/blog', {
			templateUrl : 'views/blog.html',
			controller  : 'blogController'
		});
});



app.controller('homeController', function ($scope) {
	$scope.message = 'Home';
});

app.controller('aboutController', function ($scope) {
	$scope.message = 'About';
	
});

app.controller('portfolioController', function ($scope, $http, $modal, $log) {
	$scope.message = 'Portfolio';
	
	$http.get('data/portfolio/project.json')
	.success(function (data) {
		$scope.portfolio = data;
	});

	$scope.items = ['item1', 'item2', 'item3'];

  	$scope.open = function (project) {

	    var modalInstance = $modal.open({
	      templateUrl: 'myModalContent.html',
	      controller: ModalInstanceCtrl,
	      size: 'lg',
	      resolve: {
	      project: function () {
	          return project;
	        }
	      }
	    });
  	};
});

app.controller('blogController', function ($scope) {
	$scope.message = 'Blog';
});


// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

var ModalInstanceCtrl = function ($scope, $modalInstance, project) {

  $scope.project = project;

  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};