var gulp = require('gulp');

var jshint = require('gulp-jshint');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var minifyHTML = require('gulp-minify-html')
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var connect = require('gulp-connect');
var angular = require('gulp-angular-templatecache');

gulp.task('jshint', function() {
	gulp.src(['./app/js/*.js', 
		'./app/js/controllers/*.js',
		'./app/js/directives/*.js',
		'./app/js/services/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('imagemin', function() {
	var imgSrc = './app/img/**/*',
		imgDst = './build/img';

	gulp.src(imgSrc)
		.pipe(changed(imgDst))
		.pipe(imagemin())
		.pipe(gulp.dest(imgDst));
});

gulp.task('htmlpage', function() {
	var htmlSrc = './app/*.html',
		htmlDst = './build';

	gulp.src(htmlSrc)
		.pipe(changed(htmlDst))
		.pipe(minifyHTML())
		.pipe(gulp.dest(htmlDst));
});

gulp.task('scripts', function() {
	gulp.src(['./app/js/*.js', 
		'./app/js/controllers/*.js',
		'./app/js/directives/*.js',
		'./app/js/services/*.js'])
		.pipe(concat('app.js'))
		.pipe(gulp.dest('./build/js'));
});

gulp.task('styles', function() {
	gulp.src(['./app/css/*.css'])
		.pipe(concat('app.css'))
		.pipe(autoprefix('last 2 versions'))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./build/css'));
});

gulp.task('watch', function() {
	gulp.watch(['./app/**/*.html',
				'./app/**/*.js',
				'./app/**/*.css'], 
		function(event) {
			return gulp.src(event.path)
			.pipe(connect.reload());
	});
});

gulp.task('connect', function() {
	connect.server({
		root:'./app',
		port: 9000,
		livereload: true 
	})
});

gulp.task('default', ['connect', 'watch']);