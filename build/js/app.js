var app = angular.module('portfolioApp', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : './home.html',
			controller  : 'homeController'
		})
		.when('/about', {
			templateUrl : 'about.html',
			controller  : 'aboutController'
		})
		.when('/portfolio', {
			templateUrl : './portfolio.html',
			controller  : 'portfolioController'
		})			
		.when('/blog', {
			templateUrl : 'blog.html',
			controller  : 'blogController'
		})
		.otherwise({ redirectTo: '/' });
});



app.controller('homeController', function ($scope) {
	$scope.message = 'Home';
});

app.controller('aboutController', function ($scope) {
	$scope.message = 'About';
	
});

app.controller('portfolioController', function ($scope) {
	$scope.message = 'Portfolio';
	
});

app.controller('blogController', function ($scope) {
	$scope.message = 'Blog';
});




